import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

data class Product(
    val sku: String,
    var replacedBy: Product?,
    val description: String,
    val mad: Instant,
    val segment: Segment,
    val ingredients: List<Ingredient>,
    val logistics: Logistics
) : UniqueNamed(sku) {

    /**
     * walk the replacedBy chain to get the latest version of this product
     * 1. if not replaceBy is available, take the current product
     * 2. there can be several levels of replacement
     */
    fun getFinalReplacor():Product{
        TODO()
    }

    override fun toString() =
        """$sku [=> ${replacedBy?.sku ?: "-"}] ${segment.description} $description ${dateFormatter.format(mad)}"""

    companion object {
        var dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("UTC"))
    }
}