import referentials.personList

fun main() {
    val myNumbers = listOf(1, 2, 3, 5, 8, 13)
    println(myNumbers.sum())
    println(myNumbers.maxOrNull())
}