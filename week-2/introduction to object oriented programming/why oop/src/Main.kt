fun main() {
   val anObject = MyFirstClass("Accelerate")
   anObject.printStuff()
}

class MyFirstClass(private val name:String){

   fun printStuff(){
      println("$name is cool !")
   }
}