package models

import java.time.LocalDate
import java.time.Period

data class Person(
    val id: String,
    val firstName: String,
    val lastName: String,
    val birthDate: LocalDate
) {
    /**
     * get the age.
     * By default, we get today's age, but we can also pass a parameter to ask age at a given date.
     * This is very useful for testing, as a test working today would fail in the future.
     */
    fun age(at: LocalDate = LocalDate.now()) =
        Period.between(birthDate, at).years

    override fun toString(): String {
        return "[$id] $firstName $lastName $birthDate"
    }
}

