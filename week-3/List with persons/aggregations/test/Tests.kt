import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test
import java.time.LocalDate
import java.time.Month

class Test {
    @Test fun testGetMaxAge() {
        val today = LocalDate.of(2022, Month.JANUARY, 10)

        val got = getMaxAge(today)

        assertThat(got).isEqualTo(37)
    }

    @Test fun testGetMinAge() {
        val today = LocalDate.of(2022, Month.JANUARY, 10)

        val got = getMinAge(today)

        assertThat(got).isEqualTo(3)
    }

    @Test fun testGetAgeRange() {
        val today = LocalDate.of(2022, Month.JANUARY, 10)

        val got = getAgeRange(today)

        assertThat(got).isEqualTo(34)
    }
}