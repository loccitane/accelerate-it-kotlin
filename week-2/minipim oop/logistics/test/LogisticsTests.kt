import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class Test {
    private val logistics1 = Logistics(
        height = 2.0,
        width = 3.0,
        length = 5.0,
        netWeight = 42.0,
        grossWeight = 63.0
    )

    @Test
    fun `should compute the volume`(){
        val vol = logistics1.volume()
        assertThat(vol).isEqualTo(30.0)
    }

    @Test
    fun `should compute the volumic mass`(){
        val vol = logistics1.volumicMass()
        assertThat(vol).isCloseTo(2.1, Assertions.withinPercentage(1))
    }
}