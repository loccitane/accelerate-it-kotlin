# Person list

For this section, we will work with `Person`, defined in `models/Person.kt`.

For example:
```
   val paf = Person(id="001", firstName="Paf", lastName="Dog", birthDate=LocalDate.parse("1984-05-01"))
   val flipflap = Person(id="002", firstName="Fliflap", lastName="Girafe", birthDate=LocalDate.parse("1984-04-23"))
   val grouik = Person(id="003", firstName="Grouik", lastName="Frog", birthDate=LocalDate.parse("2001-11-12"))
```

## Building a list

We want to build a list of persons `myList:List[Person]` (where `List[Person]` is the list class).
Kotlin offers us a convenient way:
```
   val myList = listOf(paf, flipflpa, grouik)
```

For your convenience, we provide you with such a list `personList`, that you can check in `referentials/PersonRef`.

## Counting elements in a list

How many elements are in a list? Kotlin `List` class offers us a `.size` property

```
   val nbPersons = myList.size
```

### Exercise #1

Go to `Task.kt` and fill the `countPersonInReferential` function.

## Accessing one element

It can be useful, sometimes, to access to a ith element in the list.

**NB** the first element as an index 0

To get the third element:

```
  val p = myList[2]
```