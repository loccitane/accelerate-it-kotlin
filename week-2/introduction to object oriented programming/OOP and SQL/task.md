# OOP and SQL

We know how OOP defines a data structure and allows us to create instances.

This may remind you of SQL, when you were creating tables (`CREATE TABLE...`) and then inserting rows (`INSERT INTO...`)