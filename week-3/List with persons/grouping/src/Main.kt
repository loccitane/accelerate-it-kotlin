import kotlin.math.floor
import kotlin.math.log10

val log10Categ: (Long) -> Int = { x -> floor(log10(x.toDouble())).toInt() }

fun main() {
    // fibos <- [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, ...]
    val fibos = fibonacciNumbers(80)

    // print the 20 first numbers
    println(fibos.take(20))

    val countByLog10 = fibos
        // group by the log10 (integer) bracket
        // Instead of passing a lambda between {}, we can also pass directly a function
        // mapOf(0 -> [1,1,2,3,5,8], 1 -> [13,21,34,55,89], 2 ->[144,233,377,610,987], ...)
        .groupingBy(log10Categ)
        // count the number of elements in each log10 bracket
        // mapOf(0 -> 6, 1 -> 5, 2 -> 5, ...)
        .eachCount()

    // And we print
    // to our wonder, we see that fibonacci number are evenly distributed in brackets [10^n, 10^(n+1)[
    countByLog10.forEach { (l, n) ->
        println("$l\t$n")
    }
}

/*
Get a list with the n first Fibonnaci numbers
https://en.wikipedia.org/wiki/Fibonacci_number
 */
fun fibonacciNumbers(n: Int): List<Long> {
    return (1..n - 1)
        .scan(
            // start with a pair of two initial values
            (1L to 1L),
            // the next value is the sum of the precedent
            { acc, _ -> acc.second to (acc.first + acc.second) }
        ) // at this stage, we have listOf(1 to 1, 1 to 2, 2 to 3, 3 to 5, 5 to 8, ...)
        //We can now take the first element of each pair
        .map { p -> p.first }
}