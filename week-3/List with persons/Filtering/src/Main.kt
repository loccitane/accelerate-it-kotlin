import referentials.personList

fun main() {
    val myNumbers = listOf(1,2,3,5,8,13)

    val evenNumbers = myNumbers.filter{x -> x%2 == 0}

    println(evenNumbers)
}