# Ingredient [INTERMEDIATE]

It is time to work with ingredients, which have a name (chemical compound) and relates to a list of families (e.g "alcohol", "allergens"...)

We see two classes arriving:
 * `Ìngredient` (with the two properties)
 * `IngredientReferential` which hold the list of all ingredients, referenced by their name

## Exercises

1. In `Ingredient`, fill the method to know the number of families for a given ingredient
<div class="hint">
Go back to the section on collection to find how you can know a list size.
</div> 

2. In `IngredientReferential` fill the methods to count the number of ingredients and to get one ingredient, depending on its passed name

## Where to get the data from, for IngredientReferential?

Data is loaded from file, by `IngredientReferentialLoader`, which is provided.
There are a few ingredients downloaded from git, in the `resources/minipim-ingredients-sample.rjson` file.

## Try out
You can tweak the main file in IngredientScript

