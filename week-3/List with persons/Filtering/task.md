# Filtering [INTERMEDIATE]

We have seen how to transform list and query a few aggregation functions.

It is now time to tackle yet another concept: **filtering**.

It consists in taking a list and keeping only the elements that fulfill a condition:

* The product that contains an ingredient or a has a given segment value.
* the above a certain age.

## An example with number

We want to keep the even numbers in a `List<Int>`. To know if a number is even, we take the modulo 2 and see if it is
equal to 0 : `x%2 == 0`

```
val myNumbers = listOf(1,2,3,5,8,13)

val evenNumbers = myNumbers.filter{x -> x%2 == 0}
```

As with transformations, we use the *lambda* simple function notation. In this case the lambda must return a `Boolean` (
a `true` or `false` value), to say if we shall keep the element.

With transformation, we were turning a list of some type into a list of another type (e.g. `Person` into `Int`
with `getAllAges()`), but the list had the same size. With filtering, we keep the same type, but we (eventually) reduce
the size of the list.

# And we can pipe all these functions !

The real power of these transformations, filtering, aggregations is that we can pipe them.

For example, if we want to take the `myNumbers`, keep the even ones, take the square and get the sum:

```
val magic = myNumbers
                .filter{x -> x%2 == 0}  // keep the even
                .map{ x -> x*x}         // put to the square
                .sum()                  // sum
```

The big advantage is that we can combine simple steps:

* they are testable (we can test each lambda independently)
* they are reusable
* they are readable