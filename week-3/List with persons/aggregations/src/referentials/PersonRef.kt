package referentials

import models.Person
import java.time.LocalDate

// Inspiration from https://fr.wikipedia.org/wiki/Paf_le_chien
val personList = listOf(
    Person("001", "Paf", "Dog", LocalDate.parse("1984-05-01")),
    Person("002", "Fliflap", "Girafe", LocalDate.parse("1984-04-23")),
    Person("003", "Grouik", "Frog", LocalDate.parse("2001-11-12")),
    Person("004", "Fleuteupeu", "Dolphin", LocalDate.parse("1999-12-31")),
    Person("005", "Hop", "Fox", LocalDate.parse("1999-12-31")),
    Person("006", "Creu", "Snail", LocalDate.parse("1999-12-25")),
    Person("007", "Schtroumpf", "Hamster", LocalDate.parse("2012-04-01")),
    Person("008", "Slurp", "Slug", LocalDate.parse("2018-01-15")),
)

/*
* We explicitly define the dictionary id -> person
* We also could have built it directly from personList (but we'll see that later on)
* personDictionary = personList
* .map{ p -> p.id to p}
* .toMap()
 */
val personDictionary = mapOf(
    "001" to Person("001", "Paf", "Dog", LocalDate.parse("1984-05-01")),
    "002" to Person("002", "Fliflap", "Girafe", LocalDate.parse("1984-04-23")),
    "003" to Person("003", "Grouik", "Frog", LocalDate.parse("2001-11-12")),
    "004" to Person("004", "Fleuteupeu", "Dolphin", LocalDate.parse("1999-12-31")),
    "005" to Person("005", "Hop", "Fox", LocalDate.parse("1999-12-31")),
    "006" to Person("006", "Creu", "Snail", LocalDate.parse("1999-12-25")),
    "007" to Person("007", "Schtroumpf", "Hamster", LocalDate.parse("2012-04-01")),
    "008" to Person("008", "Slurp", "Slug", LocalDate.parse("2018-01-15")),
)