import java.time.Instant

fun main() {
    println("Hello Sailor!")

    val currentTime = Instant.now()
    println("It is $currentTime")
}
