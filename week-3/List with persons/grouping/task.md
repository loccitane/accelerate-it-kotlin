# Grouping [Advanced]

It is now time to make one more step in fluent programming: grouping

We have a list and we want to group them through some functions, in order to have a list of list

## An example with Fibonacci
```
val myNumbers = listOf(1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765)
```

We want to group those numbers by log10 brackets (all the one 0 <= n 10, the ones 10 <= n <100, the ones 100 <= n <1000 ...).
There is more than one way to do it ! Check Main.kt for another more mathematical version.
But we can transform an `Int` into a `String` ( `21` -> `"21"`) and count the characters.

```
val groupIn10Brackets = myNumbers.groupBy{ n -> n.toString().length }
```

That gives us a `Map` (remember the Potter Kata), where all the numbers with the same `n.toString().length` are falling together.  

```
  groupIn10Brackets <- mapOf( 1 <- [1,1,2,3,4,8], 2 <- [13,21,34,55,89], ...)
```

We can them print them, count them  etc...

**NB**: there is more than one way to do it! `Main.kt` expresses just another one.


## Exercise [ADVANCED]

### 1 
Based on our `personList`, return a count of how many people are born per year 

### 2 
And now, count on which day we have more than one person celebrating their birthday.

**NB**: this last one is more tricky, as you have more code to write ;)