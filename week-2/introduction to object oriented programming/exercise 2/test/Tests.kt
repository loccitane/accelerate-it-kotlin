import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class Test {
    @Test
    fun `17 should not be allowed to drink by default`(){
        val p=Person("x", "y", 17)

        val got = p.canDrink()

        assertThat(got).isEqualTo(false)
    }

    @Test
    fun `18 should be allowed to drink by default`(){
        val p=Person("x", "y", 18)

        val got = p.canDrink()

        assertThat(got).isEqualTo(true)
    }

    @Test
    fun `20 should be allowed to drink by default`(){
        val p=Person("x", "y", 20)

        val got = p.canDrink()

        assertThat(got).isEqualTo(true)
    }
    @Test
    fun `20 should not be allowed to drink if age limit is 21`(){
        val p=Person("x", "y", 20)

        val got = p.canDrink(21)

        assertThat(got).isEqualTo(false)
    }

    @Test
    fun `21 should be allowed to drink if age limit is 21`(){
        val p=Person("x", "y", 21)

        val got = p.canDrink(21)

        assertThat(got).isEqualTo(true)
    }

    @Test
    fun `85 should be allowed to drink if age limit is 21`(){
        val p=Person("x", "y", 85)

        val got = p.canDrink(21)

        assertThat(got).isEqualTo(true)
    }

}