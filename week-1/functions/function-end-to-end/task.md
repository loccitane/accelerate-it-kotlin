# Function end to end [INTERMEDIATE]

## Argument and Body

We want to compute not only the volume, but the volumic mass.

Passing four arguments to a function (width, length, height, netWeight) as floating point values, we want the function to return the netWeight divided by the volume.

