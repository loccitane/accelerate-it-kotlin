# Logistics [INTRODUCTION]

During, the first week, we have considered various functions:
```
fun volume(width: Double, length:Double, height:Double): Double
fun volumicMass (width: Double, length:Double, height:Double, grossWeight:Double):Double
...
```

Now that we are used to OOP, we can define a `Logistics` class with
```
data class Logistics(
    val height: Double,
    val width: Double,
    val length: Double,
    val netWeight: Double,
    val grossWeight: Double
){
    fun volume(): Double{
        TODO()
    }
    fun volumicMass():Double{
       TODO()
    }
}
```

### Exercise

Fill the the content for the two methods and let the test pass