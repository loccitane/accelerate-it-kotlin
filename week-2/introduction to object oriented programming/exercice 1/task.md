# OOP Exercise 1 [INTRODUCTION]

Let's muscle up the `salutations()` methods we have see in the previous section:

* it is a String, concatenating `firstName` and `lastName`
* BUT, the passed string are sometimes not starting with a capital letter, and we want that fixed!
* `"firstName: paf lastName: the Dog` should be saluted `"Paf The Dog""`

Go check the test to see the executable specifications.

<div class="hint">
There are two tests, start by turning the first one green (without capitalization)
</div>

<div class="hint">
Do not forget the `return statement`
</div>

<div class="hint">
Go check the [String reference](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/) to capitalize words, except if you want to develop everything yourself.
</div>