import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class IngredientTest {
    val ing = Ingredient(
        name = "PEG-10 NONAFLUOROHEXYL DIMETHICONE COPOLYMER",
        families = listOf("PEG", "SILICONES")
    )

    @Test
    fun countFamilies() {
        val n = ing.countFamilies()

        assertThat(n).isEqualTo(2)
    }
}