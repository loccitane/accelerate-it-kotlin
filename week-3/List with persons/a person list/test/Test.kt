import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class Test {
    @Test
    fun `should return the number of person in the referential`(){
        val n = countPersonInReferential()

        assertThat(n).isEqualTo(8)

    }
}