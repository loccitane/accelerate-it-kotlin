import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class Test {
    /**
     * Test for the Main.kt
     */
    @Test
    fun testFibonacciNumbers() {
        val got = fibonacciNumbers(7)

        assertThat(got).isEqualTo(listOf<Long>(1, 1, 2, 3, 5, 8, 13))
    }

    @Test
    fun `log10Categ for 6 should return 0`() {
        val got = log10Categ(6)

        assertThat(got).isEqualTo(0)
    }

    @Test
    fun `log10Categ for 25 should return 1`() {
        val got = log10Categ(25)

        assertThat(got).isEqualTo(1)
    }

    @Test
    fun `log10Categ for 1000 should return 3`() {
        val got = log10Categ(1000)

        assertThat(got).isEqualTo(3)
    }

    @Test
    fun testCountByYearOfBirth() {
        val got = countByYearOfBirth()

        assertThat(got).isEqualTo(
            listOf(
                1984 to 2,
                1999 to 3,
                2001 to 1,
                2012 to 1,
                2018 to 1
            )
        )
    }

    @Test
    fun testBirthdayWithMultiplePersons() {
        val got = birthdayWithMultiplePersons()

        assertThat(got).isEqualTo(listOf("4/1", "12/31"))
    }
}