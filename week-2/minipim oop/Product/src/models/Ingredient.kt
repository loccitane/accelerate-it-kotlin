data class Ingredient(
    val name: String,
    val families: List<String>
) : UniqueNamed(name) {

    /**
     * @return the number of families
     *
     */
    fun countFamilies(): Int {
        return families.size
    }

    override fun toString() = "$name -> $families"
}
