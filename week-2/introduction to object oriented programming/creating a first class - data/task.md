# Creating our first class -  data [Introduction]

OOP unifies the vision of data & computation. So let's see them both with a simple example: people, who have first and
last names, and an age.

## Looking at data

We need to introduce two major concepts

* **class**
* **object**, also named **instance** (of a class)

### Concepts

#### Class

The **class** is a definition of the structure we have at hand.

For all people, we want to handle their:

* first name: a string of characters
* last name: another string of characters
* age: an integer value

In Kotlin, we define a class like:

```
data class Person(
  val firstName: String,
  val lastName: String,
  val age: Int
) {
    // we will fill some stuff here later 
}
```

**NB**: there are other ways to define a class, but let's stick to this default one

#### Instances (or objects)

We've defined the structure of a person, now let's create Person variables.

```
    val onePerson = Person("Paf", "The Dog", 42)
    val anotherPerson = Person("Flap-Flap", "The Giraffe", 24)
```

And that's it! We have two instances and can start playing with them, accessing their properties using the `.` character:
```
println(onePerson.firstName)
// -> Paf
```

By the way, we can also print the whole object:
```
println(onePerson)
// -> Person(firstName=Paf, lastName=The Dog, age=42)
```
**NB** when we `println`  an instance of Person, Kotlin tries to find a meaningful way to transform it into a String.
We'll see later on how we can tweak that.

#### Naming conventions

Each language has their particularities in naming things. In Kotlin (like Java and many others), we use **PascalCase** and **camelCase**. 
By convention, class names start with an upper case, when variables and
functions start with a lowercase.
```
// Class names use PascalCase 
data class MyVeryLongClassName {

    // Variables use camelCase
    val variableOfMyClass: Int 
    
    // Likewise, functions use camelCase
    fun printSomething() {
        println("Something")
    }
}

```

