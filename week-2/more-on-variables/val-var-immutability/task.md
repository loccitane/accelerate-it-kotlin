# About immutability (val & var) [Intermediate]

We have seen how a variable is in fact the assignment of a value to a name.

Then, instead of playing around with the value (e.g. `3.0`), we pass by a name (e.g. `width`)

For the moment we have used it through 

```
  val width = 3.0
  someFunction(width)
```

By default, functional oriented languages (such as Kotlin) advocate for immutability.
It means that it is not possible to change the variable value once it is assigned.

```
  val width = 3.0
  ...
  width = 4.0 => compilation time error
```

## `val`ue Vs `var`iable

However, in some situations, it is convenient to alter a variable.
To do so, it shall be declared with `var` instead of `val`

```
  var width = 3.0
  ...
  width = 4.0  // OK
```

## Do we need both ?
Some purely functional languages do not tolerate mutability.
But for common problems, it is convenient to be able to alter a variable once it has been created.

Our recommendation is to use `val` as much as possible... until it is not anymore.
Even though that may require some creativity, especially from developers used to the classic fully mutable paradigm.