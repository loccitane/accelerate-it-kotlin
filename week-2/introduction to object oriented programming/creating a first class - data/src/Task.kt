data class Person(
    val firstName: String,
    val lastName: String,
    val age: Int
) {
    // we will fill some stuff here later on
}

fun main() {
    val onePerson = Person("Paf", "The Dog", 42)
    val anotherPerson = Person("Flap-Flap", "The Giraffe", 24)

    println(onePerson)
    println(anotherPerson)

    println("first name: ${onePerson.firstName}")
}