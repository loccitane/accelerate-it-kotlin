data class Person(
    val firstName: String,
    val lastName: String
) {
    fun salutations(): String {
        return "$firstName $lastName"
    }
}

fun main() {
    val onePerson = Person("Paf", "The Dog")
    println(onePerson.salutations())
}