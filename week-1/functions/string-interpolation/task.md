# String [Introduction]

It is often useful to display information in text string. So let's get a introduction.

In Kotlin, a string can be defined by a suite of characters surrounded by `"`.

    val myString = "Hello Word!"

## String interpolation

We sometimes want to display variables within a string template. For example:

* `Paf` => `"Hello Paf!"`
* `Flapflap` => `"Hello Flapflap!"`

The first way would be by concatenating string:

    val name = "Paf"
    val myString = "Hello " + name +"!"

It is definitely a bit verbose, We prefer *string interpolation*, by adding directly the variable inside the string,
prefixed by a `$` sign:

    val name = "Paf"
    val myString = "Hello $name!"

**NB**: in this example, as Kotlin does not allow `!` character inside a variable name, it knows that the variable is
all the characters after the `$` and stops as soon as possible

It is also possible to add multiple variables inside a string template

    val name1 = "Paf"
    val name2 = "Flapflap"
    val myString = "Hello $name1 & $name2!"

## Exercise

We want a function to pass floating point values width length, height, netWeight and grossWeight and produce a nice
string to display

For example:

    val with = 2.0
    val length = 3.0
    val height = 7.0
    val netWeight = 65.0
    val grossWeigth = 50.0

Would produce: `"dimensions: 2.0 x 3.0 x 7.0 weight: 50.0 / 65.0"`

At this stage, the check button show a failing.
However, we can open the `run` panel in the lower hand side of IntelliJ and see that in fact, one test is passing, not the other,

It is time to read next section.

### A bit more complicated

When talking to users, our PO discover that, in many product, the gross weight is equal to the net weight.
This is a problem in data quality in our SAP system.

However, in such case, we want to display it only once

    val with = 2.0
    val length = 3.0
    val height = 7.0
    val netWeight = 50.0
    val grossWeigth = 50.0

Would produce: `"dimensions: 2.0 x 3.0 x 7.0 weight: 50.0"`

## Naming parameters

The `logisticsString` function has a lot of parameters (5!).
It is hard to remember the order when we call it.

    fun logisticString(
      width: Double,
      length: Double,
      height: Double,
      netWeight: Double,
      grossWeight: Double
    ): String {
      TODO()
    }

We can call it either with `logistricsString(2.0, 3.0, 7.0, 50.0, 65.0)` or

    logisticString(
      height = 7.0,
      width = 2.0,
      length = 3.0,
      grossWeight = 63.0
      netWeight = 50.0,
    ):

You can not that, naming the parameters is a bit more text, but it might also be more readable.
Moreover, the paramter order does not matter anymore.