# Accelerate IT Kotlin course



Here is a list of lessons & exercises to introduce Kotlin, around 3 aspects:
  * functions,
  * object oriented programming
  * collections

## How to use it

### Prerequisite

You have Intellij Edu installed with Kotlin, from [here](https://www.jetbrains.com/edu-products/download/#section=idea-Kotlin)

A git client.


### Clone & go

Clone this repository on your machine and open it with IntelliJ EDU (new > project > from existing sources)

**NB: you must open the project in Intellij with the root folder cloned from git (not a subfolder, such as `week-1`)**

On the upper left corner, select the "course" view.
![](images/intellij-menu.course.png)

You should have access to the first task
![](images/intellij-edu-first-screen.png)

You can now read the text, and `run` it to see the output.

**NB**: the first run make take a while, until everything is downloaded, compiled and so.

### Exercises

Some tasks are theory, with eventually a piece of code to execute.

But soon, you have to fly on your own!

You will need to complete pieces of code, of increasing complexity (even if we label the tasks with introduction/intermediate/advanced).

To fulfill the job, you need to replace the `TODO()` placeholders with your code, and press `check`.
![](images/intellij-edu-exercise.png)

If there are problems (and there will be some), they appear the lower panels.

Once your code is meeting the goal, the "checked" label appear, andd you can go to the next task.

#### How do the check work?

What is the machinery under the validation process?

There are **unit test**, a corner stone of software development.
You can have a look at the syntax in the `test/` directory in each task.



