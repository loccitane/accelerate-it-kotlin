import referentials.Referential
import java.time.Instant
import java.util.*

object ProductLoader {
    data class ProductImport(
        val sku: String,
        var replacedBySku: String?,
        val description: String,
        val mad: Date,
        val segment: Segment,
        val ingredientNames: List<String>,
        val logistics: Logistics
    ) : UniqueNamed(sku)

    fun loadProducts(productFilename: String, ingredientFilename: String): Referential<Product> {
        val ingredientReferential = Referential.load<Ingredient>("minipim-ingredients.rjson")
        val productImportReferential = Referential.load<ProductImport>("minipim-products.rjson")

        // products shall be imported in a few steps
        // 1. get the list from the json file and replace the ingredient names by the actual Ingredient stored in the ingredientReferential
        val products = productImportReferential.list().map { prodImport ->
            Product(
                sku = prodImport.sku,
                replacedBy = null,
                description = prodImport.description,
                mad = prodImport.mad.toInstant(),
                segment = prodImport.segment,
                ingredients = prodImport.ingredientNames.mapNotNull { ingredientReferential.get(it) },
                logistics = prodImport.logistics
            )
        }

        // 2. build the productReferential
        val productReferential = Referential.fromList(products)

        // 3. map the replacedBy (we need to have all the product, in order to find them from their sku)
        productReferential.list().forEach { prod ->
            prod.replacedBy =
                productImportReferential.get(prod.sku)!!.replacedBySku?.let { sku -> productReferential.get(sku) }
        }

        return productReferential
    }


}