fun area(width: Double, length: Double): Double {
    return width * length
}

fun areaString(width: Double, length: Double): String {
    val area = area(width, length)
    return "$width x $length -> $area"
}

fun isWiderThanLong(width: Double, length: Double): Boolean {
    return width > length
}

fun main() {
    val myArea = area(6.0, 7.0)
    println(myArea)
    // which is equivalent to:
    // println(area(6, 7))

    val myOutput = areaString(6.0, 7.0)
    println(myOutput)

    if(isWiderThanLong(6.0, 7.0)){
        print("wider than long")
    }else{
        println("longer than wide")
    }


}
