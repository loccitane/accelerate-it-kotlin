# Running a program

Let's start simple, and execute a program that will output a couple lines of text.

A piece of code is already written in (src/Main.kt).
You can execute it in two ways:
  * press the blue `run` button on the right description panel.
  * Open the the `Main.kt` file and press the green arrow on the line `fun main()`

You have just executed a program. Congratulations!

You can tweak the `Hello Sailor!` string and rerun the code.