data class Person(
    val id: String,
    val firstName: String,
    val lastName: String
)

fun main() {
    println("----------------  Map")
    val personList:List<Person> = listOf(
        Person("abc", "Paf", "The Dog"),
        Person("def", "Fliflap", "The Girafe"),
        Person("ghi", "Fleuteupeu", "The Doplhin"),
        Person("jkl", "Slurp", "The slug")
    )

    personList.forEach { p ->
        println(p)
    }
    println("${personList.size} are registered")
    println("The Second element is ${personList[2]}")
    // println("The tenth element is ${personList[10]}") -> ArrayIndexOutOfBoundsException

    println("----------------  Map")
    val personReferential:Map<String, Person> = mapOf(
        "abc" to Person("abc", "Paf", "The Dog"),
        "def" to Person("def", "Fliflap", "The Girafe"),
        "ghi" to Person("ghi", "Fleuteupeu", "The Doplhin"),
        "jkl" to Person("ghi", "Slurp", "The slug")
    )

    personReferential.forEach { p ->
        println(p)
    }
    println("${personReferential.size} are registered")
    println("""Element with key "jkl" is  ${personReferential["jkl"]}""")
    println("""Element with key "xyz" is  ${personReferential["xyz"]}""")



}