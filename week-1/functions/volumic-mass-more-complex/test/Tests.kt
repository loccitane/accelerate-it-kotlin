import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class Test {
    @Test fun `volumicMass with non-zero value`() {
        val got = volumicMass(2.0, 3.0, 7.0, 23.0)
        assertThat(got).isCloseTo(0.55, Assertions.withinPercentage(1))
    }

    @Test fun `return 0 if a dimension is 0`() {
        val got = volumicMass(0.0, 3.0, 7.0, 23.0)
        assertThat(got).isEqualTo(0.0)
    }
}