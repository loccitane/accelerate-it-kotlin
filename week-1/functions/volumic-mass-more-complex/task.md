# A function with a more complex body [INTERMEDIATE]

We have news from our beloved product owner!

It happens, for whatever reason, that a dimension (either width, length ot height) is zero.

With the first implementation, the function displays the barabarian `Infinity` value. 
It shall make sense for the mathematician within your soul, but frightening for our customer.

Therefore, we have a **new requirement**: *If one dimension is 0*, then the `volumicMass` function shall return `0`.

## How to proceed?

The function body starts to be a bit more complex.

### An example
Let's try to illustrate that with another situation.

Requirements:
  * We want a function named square1000(x), where x is an integer value
  * It shall return the square of x.
  * But, if this square value is greater than 1000, it returns 1000.

We would write it

    fun square1000(x: Int):Int{
      val xSquare = x * x
      if (xSquare > 1000){
        return 1000
      } else {
        return xSquare
      }
    }

We then learn two things:
   * we compute a local variable `xSquare` which is the square of `x`. We can name the variable as we wish. But the idea is, likewise function, the name shall be meaningful.
   * we have a `if(condition){ do_something} else {do_soemthing_else}` construct.

The if/else construct is explained [here](https://www.w3schools.com/kotlin/kotlin_conditions.php)

### And testing?
Check out `test/Tests.kt`.

In this case, we need to comply to the previous test case, but a new one was added for your, following the discussion with the PO.