# List aggregations

We have seen how to transform a collection and transform.
But we can do more.

For example, we want to apply one operation on all elements of a series.

Imagine we have a series of numbers:
```
val myNumbers = listOf(1,2,3,5,8,13)
```

We want to have the sum
```
val mySum = myNumbers.sum()         // -> 32
```

The minimum value:
```
val myMin = myNumbers.minOrNull()  // -> 1
```

**NB**: it is `minOrNull()` and not `min()` because the list could be empty.
And the choice of Kotlin folks is then to return a `null` value.

## Exercise

Fill the `getMaxAge(at: LocalDate)` and `getMinAge(at: LocalDate)` function in `Task.kt`

<div class="hint">
You can use the function `getAllAges(at: LocalDate)` that you filled in the previous section and combine with `minOrNull()`

Another solution is to investigate `minByOrNull{...}`.

</div>

<div class="hint">

The function `minOrNull()` return a nullable `Int?` in case the list is empty.
But we can say that the list `personList` is not empty and force the function to return a straight `Int` with `!!` 
</div>
