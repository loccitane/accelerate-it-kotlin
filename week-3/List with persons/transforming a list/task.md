# Transforming a collection [INTERMEDIATE]

One very powerful feature is to transform a list.

For example:
  * you receive a list of product and want to retrieve a list of their ingredients to make some stats
  * You receive a list of product from a system via JSON (or CSV) and want to create a list of `Product` objects
  * etc.

For the moment, we keep on with the `Person` ;)

## The `myList.map {...}`

To transform all the elements of a list (or a collection) into another collection of the same size, we simply want to apply a function on each of them.

For example, to extract all the `firstName`:

```
   val allFirstNames = personList.map{p -> p.firstName}
```

We see here two new things:
  * the `map` method, which can be applied on a collection. To this method, we must pass a *function, as a parameter". In fact, we pass a light version of a function, called a **lambda**.
  * `{p -> p.firstName}`: this is the lambda. A kind of very light function declaration (there is not even a name nor a `return`statement).

Of course, we can muscle up the lambda, and pass something slightly more complex.


```
   val allSalutation = personList.map{p -> 
     val firstNameCapitalize = p.firstName.capitalize()
     val lastNameCapitalize = p.lastName.capitalize()
     "$firstNameCapitalize $lastNameCapitalize"  # what is returned by the lambda is the last line
   }
```

### Exercise 1

In Task.kt, fill the `getAllAges(at: LocalDate)` function, in `Task.kt`, that should return of the ages of all persons in the list.
Remember that of an object `p` of class `Person` in `personList`, we have the `p.age(at)` method.

**NB**: we must give an `at` date, as the result for the test could become wrong, keeping just `LocalDate.now()`. 

### Exercise 2 [ADVANCED]

This one is a bit more tricky, as you will have to look for API documentation on the web

We want to fill `getAllBirthdays()`, which returns a list of birthdays as String (`"5/1", "4/23", "11/12",...`) for all element in `personList`.

So, the body of the lambda might be a bit more complex. 

You can have the `p.birthday` date, but must extract from this date, the numeric month and day of the month.
To do so, you shall go to the `LocalDate` API documentation, for example [here](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html) 
