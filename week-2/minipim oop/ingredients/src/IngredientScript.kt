fun main(){
    val ingRef = IngredientReferentialLoader.load("minipim-ingredients-sample.rjson")

    ingRef.dictionary.forEach { (_, ing) -> println(ing) }

    println("the dictionary contains ${ingRef.size()} elements")
    println("""example -> ${ingRef.get("PEG-10 NONAFLUOROHEXYL DIMETHICONE COPOLYMER")}""")
    println("""not existing -> ${ingRef.get("Paf")}""")
}