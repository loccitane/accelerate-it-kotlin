import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class Test {
    @Test fun `salutations with capitalized name should go direct`() {
        val p = Person("Paf", "The Dog")

        val got = p.salutations()

        assertThat(got).isEqualTo("Paf The Dog")
    }

    @Test fun `salutations should capitalize the first letter of each word`() {
        val p = Person("paf", "the Dog")

        val got = p.salutations()

        assertThat(got).isEqualTo("Paf The Dog")
    }
}