# Functions

A function shall remind you of maths. 
Remember those joyful moments at school, when your were blasted by the beauty of mathematics!


In simple terms, a function takes an **input** and produces an **output**.

    plus(x, y) -> z
    // z is eventually equal to x + y

Defining a function is **defining the mechanism that transform the input into an output**.

## Kotlin functions

### Types
Kotlin is a **statically typed** language, and when we think of input and output (**variables**), we must be explicit about what we mean.

Some (primary) types are:
  * `Int` for integer numbers (*e.g.*, -3, 0, 42)
  * `Double` for real numbers (*e.g.*, -3.7, 0.321, 42.0). Note that there is a `.0` even if the number is a rounded value.
  * `String` for character strings (*e.g.* "Paf the Dog"). Note the double quotes `"` around the content.
  * `Boolean` for something with is true or false. In fact, the two possible vaoues are `true` or `false`.

More types will come along the way. Even type that we will define ourselves.

### A function structure

A function, as we said above, transforms an input into an output.
So, we shall say to the compiler (the machinery transforming a human input into some output executable by a computer).
By the way, we can say that a compiler is a function.

We have to comply to a precise syntax:

    fun functionName(argument1: Type1, argument2: Type2,... argumentN: TypeN): TypeReturn

* `fun` is the keyword used to say we are declaring a function.
* `functionName` is the name we give to the function. It has to be humanly understandable.
* `argumentX: TypeX` are the input, together with their types.
* `TypeReturn` is the type of the value returned by the function.

Then, the function as a body and returm a value

    fun functionName(argument1: Type1, argument2: Type2,... argumentN: TypeN): TypeReturn {
      ....
      return someValue
    }

  * `return` is the keyword for indicating what is to be passed as output.
  * `someValue` must be of type `TypeReturn`

### An example

    fun area(width: Double, length: Double): Double {
      return width * length
    }

### Calling the function

We can therefore call the function

    val myArea1 = area(6.0, 7.0)

By doing so, we have called the function a first time with `width=6.0` and `length=7.0` and stored the results into a **variable** named `myArea1`.
We can then work with `myArea1`, to pass it to another function, to print it etc.

We can of course call the same function subsequently:

    val myArea2 = area(62.0, 7.9876)

## Why do we use functions?

We could of course write

    val myArea1 = 6.0 * 7.0
    val myArea1 = 62.0 * 7.9876
   
But:
  * a function is **reusable**. Imagine the function is a bit more complex than that. We would have to replicate the logic all over the place.
  * A function has a *meaningful* name. It implies that the next person reading the code (or yourself, in 2 months) understand what is it aimed at doing.
  * We can **test** a function. That means we can provide a list of inputs and the expected output and assert that the function fit the contract. Therefore, we are confident that this piece of code acts accordingly with the specifications.

## Designing functions

  * A function shall have no side effects (not modifying the input, nor variables outside its scope)
  * It should have a limited number of arguments (3 is a healthy limit).
  * IT should do one thing and only one thing. But it can of course call other functions if the logic is complex.

As we will see later, Kotlin provides by defaults thousands of functions.
But yet, we will still have to define our own

## Try it

Visit [src/Main,kt](src/Main.kt) to see a few functions in action!