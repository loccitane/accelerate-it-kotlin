# Logistics -design your own tests [INTERMEDIATE]

Some of you have expressed the desire to write their own tests.
So here we go!

## Packaging the product.

around a product, we need to make a box, to ship it.
The box is a parallelogram, with six faces.
We want to compute how much card board is actually needed.

```
    fun packageBoxArea(): Double{
        TODO()
    }
```

### We need overlap !

In fact, to make an actual box, we need a bit more material.
So that we can have the overlap, in order to have the box sealed properly.

Our contact at the factory told us that we need to add 20% to the minimal theoretical coverage.

### The exercise

This time, we do not have the tests ready.

So you must first write the unit tests and then code your methods!

1. Start with the first functionality
2. Compute by hand an example and the expected result
3. write the test
4. write the code
5. Go back to step 2. with the second functionality