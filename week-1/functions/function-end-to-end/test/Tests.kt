import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.withinPercentage
import org.junit.Assert
import org.junit.Test

class Test {
    @Test fun `volumicMass with non-zero value`() {
        val got = volumicMass(2.0, 3.0, 7.0, 23.0)
        assertThat(got).isCloseTo(0.55, withinPercentage(1))
    }
}