# Your first function [INTRODUCTION]

Let's extend previous section with writing your function return value.

Edit the  `TODO()` placeholder with the correct answer.

And press the `Check` button.

## How does it assess the result?

From a given input, a function shall produce a stated output.

This is formalized in **unit tests**. Those tests are executed in order to see that the behavior is correct.

You can open `test/Tests.kt` in order to see the syntax.


## Test Driven Development

When we develop a functionality, (at whatever level), the first thing to do is to write the test.

This has many advantages:
  * before diving in the solution, we first think about what the piece of code is supposed to do. This split the mental effort of thinking about the **outcome** and the **production** at once.
  * The test becomes the living documentation:
    * looking at the tests, I will understand what your code actually do
    * This documentation is always up-to-date. Else, the test would fail.
  * The test are run automatically, all the time. This ensures that you can modify the code, by adding new cases, but the previous specifications are still met.
  * You can **refactor** the code safely (for performance or else), knowing that a **safety net** will catch behavior divergence.
  * Someone else can pick up the code and modify it, confident that thheir chances of breaking something is limited.
  * **Everyone sleeps better at night.**

Manual testing a code through console print **IS NOT TESTING**. It is at best "debugging", even though there are better ways to debug.

