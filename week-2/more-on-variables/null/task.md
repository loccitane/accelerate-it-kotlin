# Null [Intermediate]

Aka *"the one billion dollar mistake"* Tony Hoare.

Let's consider the variable `middleName`, as an attribute of a person definition (together with `firstName`and `lastName`).
It is, like the two others, a `String`. However, this variable is optional, it might be undefined,
One solution is to feed it with a empty string `""`.

However, we'd like to carry the information that the variable is not defined.

If we look at another example, the `age` numerical integer value, it is different to say `0` then to say *"not defined"*.

Several options are possible to answer this other dimension.
Among them, a common one is to use `null`.

Then, instead of having a function
```
  fun salutations(
      firstName: String,
      middleName: String,
      lastName: String
  ):String{
     val 
     return "
  }
```

We can expressly state that `firstName` and `lastName` are defined, while we are not sure for `middleName`, with a `?`

```
  fun salutations(
      firstName: String,
      middleName: String?,
      lastName: String
  ):String{
  }
```
## Interacting with `null`

`null` is just a value, holding the fact that the variable is unknown.

### Not doing anything
If we have 

```
  fun salutations(
      firstName: String,
      middleName: String?,
      lastName: String
  ):String{
     return "$firstName $middleName $lastName"
  }
```
Then 
```
println("paf", null, "dog")
```
Will output
```
paf null dog
```

This is not so pretty...

We would like to manage `null` value with a empty string.

### Managing null with `if`

We could manage the null value with a explicit check

```
  fun salutations(
      firstName: String,
      middleName: String?,
      lastName: String
  ):String{
     val checkedMiddleName = if(middlename == null){
       ""
     }else{
       middleNAme
     }
     return "$firstName $checkedMiddleName $lastName"
  }
```
Then
```
println("paf", null, "dog")
```
Will output
```
paf  dog
```

### More on `?`

#### ?. for null-safe calls
If we try to call:

```
val p = Person("paf", null, "dog")

val midCap = p.middleName.capitalize() -> Compilation error 
```

It will fail (it will not even compile), as it is not possible to call the `capitalize()` method on a non existent variable.

We can issue a **null-safe** call by adding `?`. 
This means that, if the variable is `null`, then it won't call the subsequent method and return null instead.
But if the variable is not `null`, it will pass it along to the method.

```
val p1 = Person("paf", null, "dog")
val p2 = Person("paf", "the", "dog")

val midCap1 = p1.middleName?.capitalize()  -> null
val midCap2 = p2.middleName?.capitalize()  -> "The" 
```
#### `?:` for default value

We often have a default behavior in mind when encountering a `null` value (or you better have one!), as we might not want to carry null along down the way. 
One solution is to address it, as previously, with a `if(...) ... else ...` pattern.
But that it pretty verbose...

Another solution is to return a default value neatly.

Imagine that we want to capitalize our middle name or return empty string `""` if the middle name was not present.

This can be achieved with the `?:` fallback:
```
val midCap = p1.middleName?.capitalize() ?: ""
```

Which is equivalent to the (less concise):
```
val midCap =( )p1.middleName?:"").capitalize()
```

You will experiment that in the following exercise.