# Introduction to collections

In "week 2", we have made a short introduction to collections.
We have briefly seen how we can gorup element into `List` (an `Ingredient` having a list of families) or `Map` (a `IngredientReferential`, associating an ingredient name to a `Ingredient` object).

Collections are widely used set of classes and it is now time to dive a little bit deeper.

Based on this two categories of collections (many more exist), we will see:
* basic operations (construction, size, access)
* how to loop over
* transforming
* filtering
* reducing, grouping
* and a bit more

To move forward, we will balance between "simple" example with `Person` objects and then go to our MiniPIM, leveraging on the previous session.
We will first cover the main functionalities with the first topic (Person), before making a second passage with the more complex Product.

The directory (package) have a structure, mainly for the stuff we want out of our sight:
* `models/` where we put the object class definitions
* `referentials/`, where we put the list, map etc.

Then:
* `Main.kt` a file just to execute to see how things work
* `Task.kt`, where you have to fill in

To have another take at the matter (and a somewhat more complete), you can head to https://play.kotlinlang.org/byExample/05_Collections/01_List 