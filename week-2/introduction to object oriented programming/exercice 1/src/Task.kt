data class Person(
    val firstName: String,
    val lastName: String,
) {
    fun salutations(): String {
        return "${firstName.capitalize()} ${lastName}"
    }
}