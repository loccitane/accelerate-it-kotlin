data class Logistics(
    val height: Double,
    val width: Double,
    val length: Double,
    val netWeight: Double,
    val grossWeight: Double
){
    fun volume(): Double{
        TODO()
    }

    /**
     * gross weight divided by the volume
     */
    fun volumicMass():Double{
        TODO()
    }

    /**
     * what is the surface of the product box (the sum of the six side)
     */
    fun packageBoxArea(): Double{
        TODO()
    }

    /**
     * In fact, to make an actual box, we need more cardboard than just the parallelogram around our product.
     * Our experience from the factory is that we need 20% more that for the raw packageBoxArea
     */
    fun packageBoxWithOverlap(): Double{
        TODO()
    }
}