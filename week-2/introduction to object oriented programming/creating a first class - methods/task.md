# Creating a first class -  methods [Introduction]

As seen in the introduction, OOP consists in grouping data and behavior.
We have seen how to put data together, and it is now time to look into the latter.

## Person methods
Imagine we have a person `firstName="Paf"`, `lastName="The Dog"`.
We'd like to provide a salutation string concatenating both, such as: `"Paf The Dog"`.

### Solution 1 (poor): The global scope function 
One solution would be to create a function:

```
data class Person(
    val firstName: String,
    val lastName: String,
    val age: Int
) {
}

fun personSalutation(p: Person):String{
    return "${p.firstName} ${p.lastName}"
} 
```

And later call it through 
```
val onePerson= Person("Paf", "The Dog")
   
println(personSalutation(onePerson)) 
// -> "Paf The Dog" 
```

The problem though is that we have to carry the function `personSalutations` everywhere.

### Solution 2 (better): attaching function to the object

The more canonical OOP solution is to attach the function to object.
This is done through **methods**, directly tied to the object context.

```
data class Person(
    val firstName: String,
    val lastName: String
) {
    fun salutations():String{
      return "$firstName $lastName"
    } 
}

val onePerson= Person("Paf", "The Dog")
   
println(onePerson.salutations()) 
```

We can note:
  * The method is a function residing within the class definition
  * We call it through `onePerson.salutations()`, like a property (`onePerson.firstName`), but with parenthesis.
  * We do not pass a `Person` argument, as it is applied directly on the caller object
  * We do not need to name it `personSalutations` as the `Person` scope is tied with it's definition. Even if naming is purely arbitrary, it is important to keep it meaningful

## OOP is everywhere

In fact, everything is an object!

For example, `String` is a class and comes with methods.

**NB**: For *primary* types (String, Int, Boolean etc.), the constructor is simplified.

Therefore, we have properties and methods attached to a str

````
   val words = "Hello World"
   val l = words.length            // -> 11, number of characters
   val upper = words.uppercase()   // -> "HELLO WORLD"
````

You can find all the possibilities in the [reference doccumentation](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/).


