package referentials

import UniqueNamed
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File

class Referential<T : UniqueNamed>(
    val dictionary: Map<String, T>
) {
    fun size() = dictionary.size
    fun list() = dictionary.values.sortedBy { it.uniqueName }
    fun get(name: String) = dictionary[name]

    companion object {
        val mapper = jacksonObjectMapper()

        /**
         * load a file in the project root /resources directory
         */
        fun getResourceFile(filename: String) =
            File(System.getenv("PWD") + "/resources/$filename")

        /**
         * Load a file line by line and build a referential
         */
        inline fun <reified T : UniqueNamed> load(filename: String): Referential<T> {
            val lines = getResourceFile(filename).readLines()
            val objects = lines.map { line -> mapper.readValue<T>(line) }
            return Referential(
                dictionary = objects.map { obj -> obj.uniqueName to obj }.toMap()
            )
        }

        fun <T : UniqueNamed> fromList(objects: List<T>) =
            Referential(objects.associateBy { it.uniqueName })

    }
}

