data class Segment(
    val code: String,
    val description: String
)