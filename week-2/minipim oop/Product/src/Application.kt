import referentials.Referential

fun main() {
    val productReferential = ProductLoader.loadProducts(
        "minipim-ingredients.rjson",
        "minipim-products.rjson"
    )

    println("${productReferential.size()} products were loaded")

    // print the 10 first product
    productReferential.list().take(10).forEach {
        println(it)
    }

    println ("-------------")
    println(productReferential.get("29GE200A16"))
    println(productReferential.get("29GE200A16")?.replacedBy)
    println(productReferential.get("29GE200A16")?.replacedBy?.replacedBy)

}