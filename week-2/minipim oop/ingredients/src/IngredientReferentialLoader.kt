import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File

/**
 * We use this object only to populate an IngredientReferential from a file.
 * There is nothing to edit from this file
 */
object IngredientReferentialLoader {
    private val mapper = jacksonObjectMapper()


    /**
     * load a file in the project root /resources directory
     */
    private fun getResourceFile(filename: String) =
        File(System.getenv("PWD") + "/resources/$filename")

    /**
     * Load a file line by line and build an Ingredient referential
     */
    fun load(filename: String): IngredientReferential {
        val lines = getResourceFile(filename).readLines()
        val ingredients = lines.map { line -> mapper.readValue<Ingredient>(line) }
        return IngredientReferential(
            dictionary = ingredients.map { ing -> ing.name to ing }.toMap()
        )
    }
}