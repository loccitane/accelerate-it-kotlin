import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class IngredientReferentialTest {
    private val ingredientReferential = IngredientReferentialLoader.load("minipim-ingredients-sample.rjson")

    @Test
    fun `size`() {
        assertThat(ingredientReferential.size()).isEqualTo(4)
    }

    @Test
    fun `get a known one`() {
        val got = ingredientReferential.get("2-BROMO-2-NITROPROPANE-1,3 DIOL")

        assertThat(got).isEqualTo(
            Ingredient(
                name = "2-BROMO-2-NITROPROPANE-1,3 DIOL",
                families = listOf("PRESERVATIVES")
            )
        )
    }

    @Test
    fun `get an unknown one should return null`() {
        val got = ingredientReferential.get("PAF")

        assertThat(got).isNull()
    }
}