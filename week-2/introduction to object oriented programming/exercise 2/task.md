# OOP Exercise 2 [INTRODUCTION]

We can pass arguments to functions (remember `volume(...)`). 
Object methods are similar to functions as they can receive arguments, run computation and return values. 
However, they can **also** access all properties local to the object instance.

## Tweaking `salutations()`

Imagine your PO thinks our salutations string is a bit dry, and wants to append a number of exclamation marks depending on some value.

A weird request, but that is what the users prefer...

We can then add an argument to the `salutations` method.
```
data class Person(
    val firstName: String,
    val lastName: String
) {
    fun salutations(exclamationCount: Int): String {
        // .repeat(n:Int) is a method applicable on string https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.text/repeat.html
        val suffix = "!".repeat(exclamationCount)
        return "${firstName} ${lastName} $suffix"
    }
}
```
And then call:
```
    val p = Person("Flap-Flap", "The Giraffe")
    println(p.salutations(7))    // -> Flap-Flap The Giraffe!!!!!!!
```

## Can they drink?

We have a new property `age:Int` to our `Person` and the question is whether they can be served alcohol.
```
data class Person(
    val firstName: String,
    val lastName: String,
    val age: Int
)
```
### Step 1: Europe at 18
The first case we deliver is for Europe, where the limit is 18.

So, we need to fill the case with a `canDrink()` method
```
data class Person(...){
    fun canDrink():Boolean{
       TODO()
    }
}
```

**Your goal is to first fill in this method**

### Step 2: The world is more complex...

Talking with functional experts, our PO discovers the next week that Europe is not the center of the world.
In some places, the drinking age limit is 21, or even totally forbidden (limit is `Int.MAX_VALUE` ?).

We then agree to have another method `canDrink`, with an `ageLimit` parameter.

```
data class Person(...){
    fun canDrink():Boolean{
       TODO()
    }
    fun canDrink(ageLimit: Int):Boolean{
       TODO()
    }
}
```

**NB:** We have two methods with the same name! But that is perfectly possible, as their **signature** (the number and type of arguments) are different.
So depending on the argument we pass, the compiler will know which one to call.

## Step 3: Refactoring with default arguments [INTERMEDIATE]

OK. You wrote your two versions of the `canDrink` method. You're done.

But having two similar functions is not super clean.

We'd love to have a single method, that we can either call with an argument or none (and it would take `18` as the default value).

Fortunately, we can tweak the function definition: https://kotlinlang.org/docs/functions.html#default-arguments

**Revisit the code by having only one `canDrink(...)` method**

### Test Driven Development: refactoring

Test Driven development is commonly understood as a two-step process:

1. Write a test (it is  red)
2. Write the code to make the test turn green

And go back to square 1 to add another test.

But there is a third very important step: **REFACTORING**.

Once the tests are green, you know that your code is meeting the bar.
But it's not perfect (performance, design, naming etc.).
So you can now remodel it and use the third leg to TDD
3. Refactor
