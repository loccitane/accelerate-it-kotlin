# Exercising the null-safe calls [INTERMEDIATE]

We want to revisit the `salutations()` method, properly handling any null values for the `middleName`property.

**Beware**: if there is no middle name, we want only one space character `" "` between the first and last name.