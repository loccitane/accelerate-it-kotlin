import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class Test {
    @Test fun `2 x 3 x 7 should produce volume 42`() {
        val got = volume(2.0, 3.0, 7.0)

        assertThat(got).isEqualTo(42.0)
    }

    @Test fun `with a zero value should produc volume zero`() {
        val got = volume(0.0, 3.0, 7.0)

        assertThat(got).isEqualTo(0.0)
    }
}