fun salutations(
    firstName: String,
    middleName: String?,
    lastName: String
): String {
    val capFirstName = firstName.capitalize()
    val capLastName = lastName.capitalize()
    val capMiddleName = middleName?.capitalize() ?: ""

    return "$capFirstName $capMiddleName $capLastName"
}

fun main() {

    println(salutations("Paf", "The", "Dog"))
    println(salutations("Flap-Flap", null, "Giraffe"))

}
