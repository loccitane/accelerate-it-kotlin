import referentials.personList
import java.time.Instant
import java.time.LocalDate

/*
Through the import above, we have the variable personList
(defined in referentials/PersonRef.kt).
 */

/*
The list of all persons is stored in the variable personList.
Ths function must return the number of element in that list
 */
fun countPersonInReferential(): Int {
    // you shall return personList.SOMETHING
    return TODO()
}

/*
We want the list of all ages at a given moment
(could be LocalDate.now())
 */
fun getAllAges(at: LocalDate): List<Int> {
    return personList.map { p -> TODO() }
}

/*
We want a list of birthdays, as string month/day
For example, if a person is born on May, 1st 2001, it should return 5/1
 */
fun getAllBirthdays(): List<String> {
    return TODO()
}

/**
 * get the maximum age for the personList
 */
fun getMaxAge(at: LocalDate): Int {
    return TODO()
}

/**
 * get the minimum age for the personList
 */
fun getMinAge(at: LocalDate): Int {
    return TODO()
}

/**
 * get the age spread at a given date.
 * I.e. the difference between the age of the oldest and the youngest person in personList
 */
fun getAgeRange(at: LocalDate): Int{
    return TODO()
}