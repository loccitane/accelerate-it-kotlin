import models.Person
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate
import java.time.Month

class PersonTest {
    @Test
    fun age() {
        val today = LocalDate.of(2022, Month.JANUARY, 10)
        val paf = Person(
            id = "001",
            firstName = "Paf",
            lastName = "The Dog",
            birthDate = LocalDate.parse("1984-05-01")
        )

        val got = paf.age(today)

        assertThat(got).isEqualTo(37)
    }
}