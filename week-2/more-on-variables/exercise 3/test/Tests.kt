import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class Test {
    @Test
    fun `should capitalize all three names`(){
        val p = Person(
            firstName = "paf",
            middleName = "the",
            lastName = "dog"
        )

        val got = p.salutations()

        assertThat(got).isEqualTo("Paf The Dog")
    }


    @Test
    fun `should salute without middleName and a single space`(){
        val p = Person(
            firstName = "paf",
            middleName = null,
            lastName = "dog"
        )

        val got = p.salutations()

        assertThat(got).isEqualTo("Paf Dog")
    }}