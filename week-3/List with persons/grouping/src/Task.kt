import models.Person
import referentials.personList
import java.time.LocalDate

/*
Through the import above, we have the variable personList
(defined in referentials/PersonRef.kt).
 */

/*
The list of all persons is stored in the variable personList.
Ths function must return the number of element in that list
 */
fun countPersonInReferential(): Int {
    // you shall return personList.SOMETHING
    return TODO()
}

/*
We want the list of all ages at a given moment
(could be LocalDate.now())
 */
fun getAllAges(at: LocalDate): List<Int> {
    return personList.map { p -> TODO() }
}

/*
We want a list of birthdays, as string month/day
For example, if a person is born on May, 1st 2001, it should return 5/1
 */
fun getAllBirthdays(): List<String> {
    return TODO()
}

/**
 * get the maximum age for the personList
 */
fun getMaxAge(at: LocalDate): Int {
    return TODO()
}

/**
 * get the minimum age for the personList
 */
fun getMinAge(at: LocalDate): Int {
    return TODO()
}

/**
 * get the age spread at a given date.
 * I.e. the difference between the age of the oldest and the youngest person in personList
 */
fun getAgeRange(at: LocalDate): Int {
    return TODO()
}

/**
 * We want to get the person with an age lower or equal to maxAge, at a given date
 */
fun filterYoungerThan(at: LocalDate, maxAge: Int): List<Person> {
    return personList.filter { p -> TODO() }
}


/**
 * We want to keep the person from personList, with a first name that has at most 5 characters
 */
fun filterShortFirstNames(): List<Person> {
    return TODO()
}

/**
 * count how many people were born per year (sorted by year)
 */
fun countByYearOfBirth(): List<Pair<Int, Int>> {
    val groupedByYear: Map<Int, List<Person>> = TODO()

    return groupedByYear
        .map { (year, persons) -> year to persons.size }
        .sortedBy { (year, _) -> year }
}


/**
 * retrieve the birthday dates (month/day) where there is more than one person
 */
fun birthdayWithMultiplePersons(): List<String> {
    TODO()
}