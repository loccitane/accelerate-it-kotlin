# Product time [Advanced]

It is now time to put everything together and load the actual Product.
In the coming weeks, we'll dive on more diverse questions about products, so this section is compulsory.

We will glue together logistics, ingredients etc.

In this section, you can discover several more advanced topics, to be discussed together

* classes are organized into packages
* We hae generic class Referential, which serves for Product or Ingredient
* We have the notion of `abstract class UniqueNamed`, as product ot ingredient have a unique dientifier (`sku`or `name`)
* We have a multistage loader


## Exercise
### Play with Application

Download the two `.rjson` files from the Teams and put them into the project `resources`directory.

Open the `Application` file and run the main.

### A bit more hard core

This one is a bit more intricated. Go to the Product class and feed the `getFinalReplacor()` method

As usual, the `ProductTest` class will show you the test cases.

* You will need to go multiple stages down
* And there one caveat: what happen for product which have (wrongfully) a replacedBy cycle? 



