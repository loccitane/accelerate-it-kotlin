import models.Person
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate
import java.time.Month

class Test {
    @Test fun testFilterYoungerThat() {
        val today = LocalDate.of(2022, Month.JANUARY, 10)

        val got = filterYoungerThan(today, 18)

        val expected = listOf(
            Person("007", "Schtroumpf", "Hamster", LocalDate.parse("2012-04-01")),
            Person("008", "Slurp", "Slug", LocalDate.parse("2018-01-15")),
            )
        assertThat(got).isEqualTo(expected)
    }

    @Test fun testFilterShortFirstNames() {
        val got = filterShortFirstNames()

        val expected = listOf(
            Person("001", "Paf", "Dog", LocalDate.parse("1984-05-01")),
            Person("005", "Hop", "Fox", LocalDate.parse("1999-12-31")),
            Person("006", "Creu", "Snail", LocalDate.parse("1999-12-25")),
            Person("008", "Slurp", "Slug", LocalDate.parse("2018-01-15")),
        )
        assertThat(got).isEqualTo(expected)
    }

}