data class IngredientReferential(
    val dictionary: Map<String, Ingredient>
) {
    /**
     * @returns the number of ingredients in the referential
     */
    fun size(): Int {
        return TODO()
    }

    /**
     * @param the ingredient name
     * @return the referred ingredient, or null if it does not exist
     */
    fun get(ingredientName: String): Ingredient? {
        return TODO()
    }
}