import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

import java.time.Instant

class ProductTest {

    val productA = Product(
        sku = "A",
        replacedBy = null,
        description = "test",
        logistics = Logistics(2.0, 3.0, 5.0, 42.0, 63.0),
        ingredients = emptyList(),
        segment = Segment("S1", "some segment"),
        mad = Instant.now()
    )

    @Test
    fun `no replaceBy available`() {
        val got = productA.getFinalReplacor().sku

        assertThat(got).isEqualTo("A")
    }

    @Test
    fun `with one replaceBy level`() {
        val productB = productA.copy(sku = "B", replacedBy = productA)
        val got = productB.getFinalReplacor().sku

        assertThat(got).isEqualTo("A")
    }

    @Test
    fun `with three replaceBy level`() {
        val productB = productA.copy(sku = "B", replacedBy = productA)
        val productC = productA.copy(sku = "C", replacedBy = productB)
        val productD = productA.copy(sku = "D", replacedBy = productC)

        val got = productD.getFinalReplacor().sku

        assertThat(got).isEqualTo("A")
    }

    @Test
    fun `with a replacedBy cycle`() {
        val productB = productA.copy(sku = "B", replacedBy = productA)
        val productC = productA.copy(sku = "C", replacedBy = productB)
        val productD = productA.copy(sku = "D", replacedBy = productC)
        // make a cycle
        productB.replacedBy = productD

        // BAM
        val got = productD.getFinalReplacor().sku


    }

}

