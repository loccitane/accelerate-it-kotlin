# Why Object-Oriented Programming (OOP)?

During the first session, we saw how we were calling functions, with a lot of parameters.
```
  fun volume(width:Double, length:Double, height:Double, grossWeight:Double):Double{
    ...
  }
  fun volumicMass((width:Double, length:Double, height:Double, grossWeight:Double):Double{
    ...
  }
```

And quickly, we saw that we were passing around plenty of parameters and had to handle many variables. So long for Jeff Bay's [Programming Calisthenics](https://williamdurand.fr/2013/06/03/object-calisthenics/)...

Moreover, looking at the previous function call, we see that there is a logic of grouping together `width`, `length`, `height` etc. together.
Talking with some functional oriented colleagues, we can see how they relate to logistics information.

We realize that we have data (`width`, `length`, `height` etc.) and computation (`volume`, `volumicMass`, and soon more) that live together.

**The Object-Oriented Programming (OOP) paradigm offers a solution to group data & functionalities features together.**

OOP is a major paradigm in software development. We will go through a few first steps and see how we can structure our MINIPIM information.

To be open, OOP is not the only paradigm existing around. But it combines neatly with other approaches and offer widely accepted approach to structure software development. 
