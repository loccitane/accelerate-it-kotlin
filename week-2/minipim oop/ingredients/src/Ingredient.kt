data class Ingredient(
    val name: String,
    val families: List<String>
){
    /**
     * @return the number of families
     *
     */
    fun countFamilies():Int{
        return TODO()
    }
    override fun toString() = "$name -> $families"
}
