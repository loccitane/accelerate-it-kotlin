data class Logistics(
    val height: Double,
    val width: Double,
    val length: Double,
    val netWeight: Double,
    val grossWeight: Double
){
    fun volume(): Double{
        TODO()
    }

    /**
     * gross weight divided by the volume
     */
    fun volumicMass():Double{
        TODO()
    }
}