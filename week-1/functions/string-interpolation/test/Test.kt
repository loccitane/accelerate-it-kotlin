import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test

class Test {
    @Test
    fun `logisticsStrung should display all values`() {
        val got = logisticString(2.0, 3.0, 7.0, 50.0, 65.0)

        val expected = "dimensions: 2.0 x 3.0 x 7.0 weight: 50.0 / 65.0"
        assertThat(got).isEqualTo(expected)
    }

    @Test
    fun `logisticsString should display one weight when netWeight equal grossWeight`() {
        val got = logisticString(2.0, 3.0, 7.0, 50.0, 50.0)

        val expected = "dimensions: 2.0 x 3.0 x 7.0 weight: 50.0"
        assertThat(got).isEqualTo(expected)
    }
}