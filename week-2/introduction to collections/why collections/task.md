# Introduction to collections [INTRODUCTION]

Until now, we have looked at single entities: one `String`, one `Person`. Or at least handling objects one by one.

However, we very often deal with collections of objects, such as a list of products, or one product containing multiple ingredients.

Next week, we will dive more into collections, but we'll need to make an introduction to two data structures:
  * **List**: a succession of objects (think of ingredients), that we can loop through, count, find the n-th element.
  * **Map** (or dictionary): another type of list where you can access an element directly through a key. Imagine a product dictionary holding a list of products, but where you can directly find/access one item through its `SKU`. The sku is then a unique key allowing to pinpoint a value.

In other words, a map is a list, enabling to reach a specific value directly through a *key to value association*.


Before heading to miniPIM, let's make a list of our `Person` class.
Consider this slightly revised version, with an `id` attribute. For sake of this exercice consider ids are unique by design.
```
data class Person(
    val id: String,
    val firstName: String,
    val lastName: String
)
```

## List

```
    val personList:List<Person> = listOf(
        Person("abc", "Paf", "The Dog"),
        Person("def", "Flip-Flap", "The Giraffe"),
        Person("ghi", "Fleu-Teu-Peu", "The Doplhin"),
        Person("jkl", "Slurp", "The Slug")
    )
```
And we can do various things
### Loop across
```
    personList.forEach { p ->
        println(p)
    }
```
### get the size
```
    println("${personList.size} are registered")
```
### Get the n-th element
The index starts at **0** and if we call for the n-th element, n must be smaller than the lists size.
```
    println("The Second element is ${personList[2]}")

    println("The tenth element is ${personList[10]}") -> ArrayIndexOutOfBoundsException
```

## Map

For the map, we associate a key (here the unique `id:String`) to point towards the object.
Our person referential is therefore a `Map<String, Person>`

At creation time note the `to` construction.

```
    val personReferential:Map<String, Person> = mapOf(
        "abc" to Person("abc", "Paf", "The Dog"),
        "def" to Person("def", "Fliflap", "The Girafe"),
        "ghi" to Person("ghi", "Fleuteupeu", "The Doplhin"),
        "jkl" to Person("ghi", "Slurp", "The slug")
    )
```
And we do have similar actions:
### Loop
```
    personReferential.forEach { p ->
        println(p)
    }
```
# Size
```
    println("${personReferential.size} are registered")
```
# Direct access

Contrary to the list, we do not access an element through a numerical index (there is no intrinsic order in a Map), but through the *key*.
Another big change is that we can request an element which does not exist (`personReferential["xyz"]`) as it will return `null`.
```
    println("""Element with key "jkl" is  ${personReferential["jkl"]}""")   -> Person(id=ghi, firstName=Slurp, lastName=The slug)
    println("""Element with key "xyz" is  ${personReferential["xyz"]}""")   -> Null
```


## Is that all for collections?

Not at all. We'll see how we can merge, filter, group, transform, aggregate collections and much more.

Our only goal here was to be able to move forward with OOP.

(And, by the way, List and Map are classes, with plenty of methods available)
