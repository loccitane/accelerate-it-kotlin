import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test
import java.time.LocalDate
import java.time.Month

class Test {
    @Test fun `getAllAges(date) should return a list of ages`() {
        // Given
        // the personList is a global variable
        val today = LocalDate.of(2022, Month.JANUARY, 10)

        // When
        val got = getAllAges(today)

        // Then
        assertThat(got).isEqualTo(listOf(37, 37, 20, 22, 22, 22, 9, 3))
    }

    @Test fun `getAllBirthdays should return a list of birthday as string`() {
        // When
        val got = getAllBirthdays()

        // Then
        assertThat(got).isEqualTo(listOf("5/1", "4/23", "11/12", "12/31", "12/31", "12/25", "4/1", "1/15"))
    }
}